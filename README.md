# Hello

#KUBERNETES DEPLOYMENT YAML

```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: default
  name: demo-cicd
  labels:
    app: demo-cicd
spec:
  replicas: 3
  selector:
    matchLabels:
      app: demo-cicd
  revisionHistoryLimit: 2
  template:
    metadata:
      namespace: default
      labels:
        app: demo-cicd
    spec:
      containers:
        - name: demo-cicd
          image: serhat190562/hello:latest
          imagePullPolicy: Always
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: demo-cicd-svc
spec:
  ports:
  - name: http
    port: 8080
    protocol: TCP
  selector:
    app: demo-cicd
  type: ClusterIP
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  namespace: default
  name: demo-cicd
  annotations:
    kubernetes.io/ingress.class: public
spec:
  rules:
    - host: demo-cicd.chillitzel.rocks
      http:
        paths:
          - backend:
              serviceName: demo-cicd-svc
              servicePort: 8080
```
